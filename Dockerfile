FROM node:carbon

WORKDIR /app

COPY package.json config.json ./

RUN yarn install

COPY .babelrc.js gulpfile.babel.js webpack.config.js ./
COPY src/ src/

RUN yarn deploy

#---------------------------------------

FROM node:carbon

COPY node_modules/ node_modules/
COPY bin/ bin/
COPY config.json ./

ENV NODE_ENV=production

CMD "node" "bin/server"

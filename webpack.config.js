const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

const browserConfig = {
  context: path.resolve(__dirname),
  entry: './src/client/js/app.js',
  output: {
    library: 'app',
    filename: path.join('js', 'app.js'),
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          cacheDirectory: true,
        },
      },
      {
        test: /\.(html)$/,
        loader: 'html-loader',
        options: {
          attrs: [
            'input:src',
            'audio:src',
            'link:href',
          ],
        },
      },
      {
        test: /\.(css|png|jpg|mp3)$/,
        loader: 'file-loader',
        options: {
          name: '[name]-[hash].[ext]',
        },
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'src/client/index.html',
    }),
  ],
};

if (process.env.NODE_ENV === 'production') {
  const {plugins} = browserConfig;
  plugins.push(new UglifyJSPlugin());
  plugins.push(new webpack.DefinePlugin({
    'process.env.NODE_ENV': JSON.stringify('production'),
  }));
}

module.exports = browserConfig;

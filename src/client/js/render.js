import global from './global';



function spectateLoop () {
  /* eslint-disable: no-param-reassign */
  if (global.died) {
    ctx.fillStyle = '#333333';
    ctx.fillRect(0, 0, global.screenWidth, global.screenHeight);

    ctx.textAlign = 'center';
    ctx.fillStyle = '#FFFFFF';
    ctx.font = 'bold 30px sans-serif';
    ctx.fillText('You died!', global.screenWidth / 2, global.screenHeight / 2);
  }
  else if (!global.disconnected) {
    if (global.gameStart) {
      ctx.fillStyle = global.backgroundColor;
      ctx.fillRect(0, 0, global.screenWidth, global.screenHeight);

      drawgrid();
      const foodMatrix = FastIntCompression.uncompress(foods).reduce(
        (r, num, idx) => {
          if (idx % 4 === 0) {
            r.push([num]);
          } else {
            r[r.length-1].push(num);
          }
          return r;
        }, []
      )
      drawFood(foodMatrix);
      fireFood.forEach(drawFireFood);
      viruses.forEach(drawVirus);

      if (global.borderDraw) {
        drawborder();
      }
      var orderMass = [];
      for(var i=0; i<users.length; i++) {
        for(var j=0; j<users[i].cells.length; j++) {
          orderMass.push({
            nCell: i,
            nDiv: j,
            mass: users[i].cells[j].mass
          });
        }
      }
      orderMass.sort(function(obj1, obj2) {
        return obj1.mass - obj2.mass;
      });

      drawPlayers(orderMass);
      if (global.playerType === 'player') {
        socket.emit('0', window.canvas.target); // playerSendTarget "Heartbeat".
      }

    } else {
      ctx.fillStyle = '#333333';
      ctx.fillRect(0, 0, global.screenWidth, global.screenHeight);

      ctx.textAlign = 'center';
      ctx.fillStyle = '#FFFFFF';
      ctx.font = 'bold 30px sans-serif';
      ctx.fillText('Game Over!', global.screenWidth / 2, global.screenHeight / 2);
    }
  } else {
    ctx.fillStyle = '#333333';
    ctx.fillRect(0, 0, global.screenWidth, global.screenHeight);

    ctx.textAlign = 'center';
    ctx.fillStyle = '#FFFFFF';
    ctx.font = 'bold 30px sans-serif';
    if (global.kicked) {
      if (reason !== '') {
        ctx.fillText('You were kicked for:', global.screenWidth / 2, global.screenHeight / 2 - 20);
        ctx.fillText(reason, global.screenWidth / 2, global.screenHeight / 2 + 20);
      }
      else {
        ctx.fillText('You were kicked!', global.screenWidth / 2, global.screenHeight / 2);
      }
    }
    else {
      ctx.fillText('Disconnected!', global.screenWidth / 2, global.screenHeight / 2);
    }
  }
}

export default spectateLoop;

const {app, setupGame, setupTimers} = require('./server');
const c = require('../../config.json');

// Don't touch, IP configurations.
var ipaddress = process.env.OPENSHIFT_NODEJS_IP || process.env.IP || c.host;
var serverport = process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || c.port;
app.listen( serverport, ipaddress, function() {
  console.log('[DEBUG] Listening on ' + ipaddress + ':' + serverport);
  console.log('[DEBUG] configuration', c);
  setupGame();
  setupTimers();
});

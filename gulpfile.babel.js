 import fs from 'fs';
import gulp from 'gulp';
import babel from 'gulp-babel';
//import jshint from 'gulp-jshint';
import nodemon from 'gulp-nodemon';
import util from 'gulp-util';
import mocha from 'gulp-mocha';
import todo from 'gulp-todo';
import gulpWebpack from 'webpack-stream';
import webpack from 'webpack';
import webpackConfig from './webpack.config.js';


export const lint = () =>
    gulp.src(['src/**/*.js'])
    .pipe(jshint({
        esnext: true
    }))
    .pipe(jshint.reporter('default', { verbose: true}))
    .pipe(jshint.reporter('fail'));


export const test = () =>
    gulp.src(['test/**/*.js'])
    .pipe(mocha());


export const buildClient = () =>
  gulp.src('src/client/js/app.js')
  .pipe(gulpWebpack({
    // jshint ignore:start
    ...webpackConfig,
    // jshint ignore:end
    mode: process.NODE_ENV === 'production' ? 'production' : 'development',
  }, webpack))
  .pipe(gulp.dest('bin/client/'));


export const buildServer = () =>
    gulp.src(['src/server/**/*.*', 'src/server/**/*.js'])
    .pipe(babel())
    .pipe(gulp.dest('bin/server/'));

export const build = gulp.parallel(buildClient, buildServer);

export const watch = gulp.series(
  build,
  () => nodemon({
    delay: 10,
    script: 'bin/server/index.js',
    ignore: 'bin/**/*',
    args: ["config.json"],
    ext: 'html js css',
    tasks: ['buildClient', 'buildServer'],
  })
);

const todoTask = () =>
    gulp.src('src/**/*.js')
    .pipe(todo())
    .pipe(gulp.dest('./'));

const todoAndLint = gulp.series(lint, todoTask);

export {todoAndLint as todo};

export default build;
